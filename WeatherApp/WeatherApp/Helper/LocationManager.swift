//
//  LocationManager.swift
//  WeatherApp
//
//  Created by Apple on 20/11/23.
//

import Foundation
import CoreLocation

protocol LocationServiceDelegate: AnyObject {
    func userUpdatedLocation(location: CLLocation)
    func locationServicesEnabled()
    func locationServicesDisabled()
}
class LocationService: NSObject {
    private let locationManager = CLLocationManager()
    private var locDelegate: LocationServiceDelegate?
    func startLocationTracking(locDelegate: LocationServiceDelegate?) {
        self.locDelegate = locDelegate
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.distanceFilter = 100
        DispatchQueue.global().async { [weak self] in
            guard let self else { return }
            if CLLocationManager.locationServicesEnabled() {
                switch locationManager.authorizationStatus {
                case .notDetermined:
                    locationManager.requestWhenInUseAuthorization()
                case  .restricted, .denied:
                    manageLocationServicesDiabledCondition()
                case .authorizedAlways, .authorizedWhenInUse:
                    startLocationUpdates()
                    self.locDelegate?.locationServicesEnabled()
                @unknown default:
                    break
                }
            } else {
                manageLocationServicesDiabledCondition()
            }
        }
    }
    
    func manageLocationServicesDiabledCondition() {
        locDelegate?.locationServicesDisabled()
    }
     
    func startLocationUpdates() {
        locationManager.startUpdatingLocation()
    }
    func stopLocationUpdates() {
        locationManager.stopUpdatingLocation()
    }
}
extension LocationService: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .authorizedWhenInUse,
             .authorizedAlways:
                startLocationUpdates()
             locDelegate?.locationServicesEnabled()
        case .restricted, .denied:
            locDelegate?.locationServicesDisabled()
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        locDelegate?.userUpdatedLocation(location: location)
        let locationAge: TimeInterval = -location.timestamp.timeIntervalSinceNow
        if locationAge > 5.0 {
            return
        }
        stopLocationUpdates()
    }
}
 
