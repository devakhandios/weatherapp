//
//  WeatherModel.swift
//  WeatherApp
//
//  Created by Apple on 17/11/23.
//

import Foundation

struct WeatherModel: Codable {
    let weather: [Weather]
    let main: Main
    let name: String
    var cityTemprature: String {
        return String(format: "%.1f", main.temp)
    }
    var description: String {
        return weather.first?.description ?? ""
    }
    var icon: String {
        return weather.first?.icon ?? "help"
    }
}

struct Main: Codable {
    let temp: Double
}

struct Weather: Codable {
    let main, description, icon: String
}
