//
//  WeatherLandingVC.swift
//  WeatherApp
//
//  Created by Apple on 17/11/23.
//

import UIKit
import CoreLocation

class WeatherLandingVC: UIViewController {
    @IBOutlet private weak var weatherView: WeatherView!
    private lazy var viewModel: WeatherViewModelProtocol = WeatherViewModel(serviceHandler: WeatherServices(), locationService: LocationService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModelBinding()
    }
    
    @IBAction func cityTapped(_ sender: Any) {
        self.navigationController?.pushViewController(CityWeatherVC(), animated: true)
    }
}
extension WeatherLandingVC {
    private func viewModelBinding() {
        viewModel.getLoadingState = { [weak self] state in
            guard let self else { return }
            switch state {
            case .populated(let weather):
                weatherView.setupWeatherView(weather)
            case .error(let error):
                debugPrint(error)
            }
        }
    }
}
