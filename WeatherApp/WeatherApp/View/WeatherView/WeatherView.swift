//
//  WeatherView.swift
//  WeatherApp
//
//  Created by Apple on 17/11/23.
//

import UIKit

final class WeatherView: UIView {
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var cityName: UILabel!
    @IBOutlet private weak var temprature: UILabel!
    @IBOutlet private weak var weatherDescription: UILabel!
    @IBOutlet private weak var weatherImage: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        initNib()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initNib()
    }

    private func initNib() {
        let bundle = Bundle(for: WeatherView.self)
        bundle.loadNibNamed("WeatherView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
    }

    func setupWeatherView(_ weather: WeatherModel) {
        
        DispatchQueue.main.sync {
            cityName.text = weather.name.capitalized
            temprature.text = weather.cityTemprature
            weatherDescription.text = weather.description.capitalized
            weatherImage.image = UIImage(named: weather.icon)
        }
    }
}
