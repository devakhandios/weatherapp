//
//  CityWeatherVC.swift
//  WeatherApp
//
//  Created by Apple on 17/11/23.
//

import UIKit

class CityWeatherVC: UIViewController {
    @IBOutlet private weak var weatherView: WeatherView!
    @IBOutlet private weak var searchField: UITextField!
    private lazy var viewModel: WeatherViewModelProtocol = WeatherViewModel(serviceHandler: WeatherServices(), locationService: LocationService())

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModelBinding()
    }
}

extension CityWeatherVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchField.endEditing(true)
        if let city = searchField.text {
            viewModel.fetchWeather(forCity: city)
        }
        searchField.text = ""
        return true
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text?.isEmpty == false {
            return true
        } else {
            return false
        }
    }
}

extension CityWeatherVC {
    private func viewModelBinding() {
        viewModel.getLoadingState = { [weak self] state in
            guard let self else { return }
            switch state {
            case .populated(let weather):
                weatherView.setupWeatherView(weather)
            case .error(let error):
                debugPrint(error)
            }
        }
    }
}
