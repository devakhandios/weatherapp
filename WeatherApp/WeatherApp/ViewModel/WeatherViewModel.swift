//
//  WeatherViewModel.swift
//  WeatherApp
//
//  Created by Apple on 17/11/23.
//

import Foundation
import CoreLocation

enum WeatherState {
    case error(Error)
    case populated(WeatherModel)
}

protocol WeatherViewModelProtocol {
    func fetchWeather(forCity cit: String)
    var getLoadingState: ((WeatherState) -> Void) { get set }
}

class WeatherViewModel: WeatherViewModelProtocol {
    private let serviceHandler: WeatherServicesProtocol
    private var locationService: LocationService
    var getLoadingState: ((WeatherState) -> Void) = {_ in }
    init(serviceHandler: WeatherServicesProtocol, locationService: LocationService) {
        self.serviceHandler = serviceHandler
        self.locationService = locationService
        startLocationTracking()
    }

    func fetchWeather(forCity cit: String) {
        serviceHandler.fetchWeather(forCity: cit) { [weak self] result in
            guard let self else { return }
            switch result {
            case .success(let weather):
                self.getLoadingState(.populated(weather))

            case .failure(let error):
                self.getLoadingState(.error(error))
            }
        }
    }

    private func fetchWeather(for lat: String, lon: String) {
        serviceHandler.fetchWeather(forLat: lat, lon: lon) { [weak self] result in
            guard let self else { return }
            switch result {
            case .success(let weather):
                self.getLoadingState(.populated(weather))

            case .failure(let error):
                self.getLoadingState(.error(error))
            }
        }
    }
}
extension WeatherViewModel: LocationServiceDelegate {
    internal func startLocationTracking() {
        locationService.startLocationTracking(locDelegate: self)
    }
    
    internal func stopLocationTracking() {
        locationService.stopLocationUpdates()
    }
    
    func userUpdatedLocation(location: CLLocation) {
        fetchWeather(for: String(location.coordinate.latitude),
                               lon: String(location.coordinate.longitude))
        stopLocationTracking()
    }
    
    func locationServicesEnabled() {
    }
    
    func locationServicesDisabled() {
    }
}
