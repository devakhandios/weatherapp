//
//  WeatherServices.swift
//  WeatherApp
//
//  Created by Apple on 17/11/23.
//

import Foundation

protocol WeatherServicesProtocol: AnyObject {
    func fetchWeather(forCity: String, completion: @escaping(Result<WeatherModel, WeatherServiceError>) -> Void)
    func fetchWeather(forLat lat: String, lon: String, completion: @escaping(Result<WeatherModel, WeatherServiceError>) -> Void)
}

class WeatherServices: WeatherServicesProtocol {
    func fetchWeather(forLat lat: String, lon: String, completion: @escaping(Result<WeatherModel, WeatherServiceError>) -> Void) {
        var urlComponent = URLComponents(string: baseURL)
        let queryItems = [URLQueryItem(name: "lat", value: lat),
                          URLQueryItem(name: "lon", value: lon),
                          URLQueryItem(name: "units", value: "metric"),
                          URLQueryItem(name: "appid", value: apiKey) ]
        urlComponent?.queryItems = queryItems

        guard let url = urlComponent?.url else {
            return completion(.failure(.badURL))
        }

        NetworkManager().fetchRequest(type: WeatherModel.self, url: url, completion: completion)
    }

    func fetchWeather(forCity city: String,
                      completion: @escaping(Result<WeatherModel, WeatherServiceError>) -> Void) {
        var urlComponent = URLComponents(string: baseURL)
        let queryItems = [URLQueryItem(name: "q", value: city),
                          URLQueryItem(name: "units", value: "metric"),
                          URLQueryItem(name: "appid", value: apiKey)]
        urlComponent?.queryItems = queryItems
        guard let url = urlComponent?.url else {
            return completion(.failure(.badURL))
        }

        NetworkManager().fetchRequest(type: WeatherModel.self, url: url, completion: completion)
    }
}
