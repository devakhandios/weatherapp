//
//  ResponseManager.swift
//  WeatherApp
//
//  Created by Apple on 20/11/23.
//

import Foundation

protocol ResponseHandlerProtocol: AnyObject {
    func fetchModel<T: Codable>(type: T.Type, data: Data, completion: (Result<T, WeatherServiceError>) -> Void)
}

class ResponseHandler: ResponseHandlerProtocol {
    func fetchModel<T: Codable>(type: T.Type, data: Data, completion: (Result<T, WeatherServiceError>) -> Void) {
        let commentResponse = try? JSONDecoder().decode(type.self, from: data)
        if let commentResponse = commentResponse {
            return completion(.success(commentResponse))
        } else {
            completion(.failure(.decodingError))
        }
    }
}
