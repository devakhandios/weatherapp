//
//  APIRequestManager.swift
//  WeatherApp
//
//  Created by Apple on 20/11/23.
//

import Foundation

protocol APIHandlerProtocol: AnyObject {
    func fetchData(url: URL, completion: @escaping(Result<Data, WeatherServiceError>) -> Void)
}

class APIHandler: APIHandlerProtocol {
    func fetchData(url: URL, completion: @escaping(Result<Data, WeatherServiceError>) -> Void) {
        URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data, error == nil else {
                return completion(.failure(.noData))
            }
            completion(.success(data))

        }.resume()
    }
}
