//
//  NetworkManager.swift
//  WeatherApp
//
//  Created by Apple on 17/11/23.
//

import Foundation

enum WeatherServiceError: Error {
    case badURL
    case noData
    case decodingError
}

class NetworkManager {
    let aPIHandler: APIHandlerProtocol
    let responseHandler: ResponseHandlerProtocol

    init(aPIHandler: APIHandlerProtocol = APIHandler(),
         responseHandler: ResponseHandlerProtocol = ResponseHandler()) {
        self.aPIHandler = aPIHandler
        self.responseHandler = responseHandler
    }

    func fetchRequest<T: Codable>(type: T.Type, url: URL, completion: @escaping(Result<T, WeatherServiceError>) -> Void) {

        aPIHandler.fetchData(url: url) { result in
            switch result {
            case .success(let data):
                self.responseHandler.fetchModel(type: type, data: data) { decodedResult in
                    switch decodedResult {
                    case .success(let model):
                        completion(.success(model))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
