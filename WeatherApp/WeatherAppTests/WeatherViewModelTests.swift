//
//  WeatherViewModelTests.swift
//  WeatherAppTests
//
//  Created by Apple on 17/11/23.
//

import XCTest
@testable import WeatherApp

final class WeatherViewModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testWeatherViewModelForCity() throws {
        let delayExpectation = XCTestExpectation()
        delayExpectation.isInverted = true
        let weatherViewModel = WeatherViewModel()
        weatherViewModel.getLoadingState = { state in
            switch state {
            case .populated(let weather):
                XCTAssertTrue(weather.name == "Dubai")
            case .error(let error):
                debugPrint(error)
            }
        }
        weatherViewModel.fetchWeather(forCity: "Dubai")
        XCTWaiter(delegate: self).wait(for: [delayExpectation], timeout: 2)
    }
    
    func testWeatherViewModelWithCordinate() throws {
        let delayExpectation = XCTestExpectation()
        delayExpectation.isInverted = true
        let weatherViewModel = WeatherViewModel()
        weatherViewModel.getLoadingState = { state in
            switch state {
            case .populated(let weather):
                XCTAssertTrue(weather.name == "Ash Shindaghah")
            case .error(let error):
                debugPrint(error)
            }
        }
        weatherViewModel.fetchWeather(for: "25.2009", lon: "55.2717")
        XCTWaiter(delegate: self).wait(for: [delayExpectation], timeout: 2)
    }
}
